package pm.c7.ssc;

import com.mojang.brigadier.tree.LiteralCommandNode;
import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.registry.CommandRegistry;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import pm.c7.ssc.commands.BackCommand;
import pm.c7.ssc.commands.SpawnCommand;

public class SimpleServerCommands implements DedicatedServerModInitializer {
    @Override
    public void onInitializeServer() {
        registerCommands();
    }

    private void registerCommands() {
        CommandRegistry.INSTANCE.register(true, dispatcher -> {
            LiteralCommandNode<ServerCommandSource> spawn = CommandManager.literal("spawn")
                    .executes(new SpawnCommand())
                    .build();

            LiteralCommandNode<ServerCommandSource> back = CommandManager.literal("back")
                    .executes(new BackCommand())
                    .build();

            dispatcher.getRoot().addChild(spawn);
            dispatcher.getRoot().addChild(back);
        });
    }
}

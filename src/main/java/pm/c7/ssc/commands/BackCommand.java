package pm.c7.ssc.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.raphydaphy.crochet.data.PlayerData;
import net.minecraft.client.network.packet.PlayerPositionLookS2CPacket;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.level.LevelProperties;

import java.util.EnumSet;

public class BackCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        ServerPlayerEntity player = context.getSource().getPlayer();

        CompoundTag toGoBackTo;
        if (PlayerData.get(player, "simpleservercommands").get("LastPos") != null) {
            toGoBackTo = (CompoundTag) PlayerData.get(player, "simpleservercommands").get("LastPos");
        } else {
            context.getSource().sendFeedback(new LiteralText("Nowhere to go back to.").formatted(Formatting.RED), false);
            return 0;
        }

        CompoundTag lastPos = new CompoundTag();
        lastPos.putDouble("X", player.getX());
        lastPos.putDouble("Y", player.getY());
        lastPos.putDouble("Z", player.getZ());
        lastPos.putString("Dim", player.dimension.toString());
        PlayerData.get(player, "simpleservercommands").put("LastPos", lastPos);
        PlayerData.markDirty(player);

        player.stopRiding();
        if (player.isSleeping()) {
            player.wakeUp(true, true);
        }

        ServerWorld targetWorld = context.getSource().getMinecraftServer().getWorld(DimensionType.byId(new Identifier(toGoBackTo.getString("Dim"))));

        if (player.world == targetWorld) {
            player.networkHandler.teleportRequest(toGoBackTo.getInt("X"), toGoBackTo.getInt("Y"), toGoBackTo.getInt("Z"), player.yaw, player.pitch, EnumSet.noneOf(PlayerPositionLookS2CPacket.Flag.class));
        } else {
            player.teleport(targetWorld, toGoBackTo.getInt("X"), toGoBackTo.getInt("Y"), toGoBackTo.getInt("Z"), player.yaw, player.pitch);
        }

        context.getSource().sendFeedback(new LiteralText("Teleported to last position."), true);

        return 1;
    }
}
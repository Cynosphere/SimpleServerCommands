package pm.c7.ssc.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.raphydaphy.crochet.data.PlayerData;
import net.minecraft.client.network.packet.PlayerPositionLookS2CPacket;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.level.LevelProperties;

import java.util.EnumSet;

public class SpawnCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        ServerPlayerEntity player = context.getSource().getPlayer();
        LevelProperties props = context.getSource().getWorld().getLevelProperties();
        ServerWorld overworld = context.getSource().getMinecraftServer().getWorld(DimensionType.OVERWORLD);

        CompoundTag lastPos = new CompoundTag();
        lastPos.putDouble("X", player.getX());
        lastPos.putDouble("Y", player.getY());
        lastPos.putDouble("Z", player.getZ());
        lastPos.putString("Dim", player.dimension.toString());
        PlayerData.get(player, "simpleservercommands").put("LastPos", lastPos);
        PlayerData.markDirty(player);

        player.stopRiding();
        if (player.isSleeping()) {
            player.wakeUp(true, true);
        }

        if (overworld == player.world) {
            player.networkHandler.teleportRequest(props.getSpawnX(), props.getSpawnY(), props.getSpawnZ(), player.yaw, player.pitch, EnumSet.noneOf(PlayerPositionLookS2CPacket.Flag.class));
        } else {
            player.teleport(overworld, props.getSpawnX(), props.getSpawnY(), props.getSpawnZ(), player.yaw, player.pitch);
        }

        context.getSource().sendFeedback(new LiteralText("Teleported to spawn."), true);

        return 1;
    }
}
